

// const ListOfUsers = [{
//     "name": "Jane",
//     "age": 36,
//     "rights": ["admin", "editor", "contributor"]
// }, {
//     "name": "Emily",
//     "age": 23,
//     "rights": ["editor"]
// },
// {
//     "name": "Paul",
//     "age": 44,
//     "rights": ["contributor"]
// }, {
//     "name": "Mark",
//     "age": 26,
//     "rights": ["contributor"]
// },
// {
//     "name": "Edward",
//     "age": 39,
//     "rights": ["admin"]
// }, {
//     "name": "Lucy",
//     "age": 23,
//     "rights": ["admin", "editor", "contributor"]
// }]

// Q . Find all the users who have admin rights.
// Q . Find all the users who are above 18 and are contributers.
// Q . Find all the users having all the rights.
// Q . Sort the users based on admin rights and alphabetical order.
// Q . Find all the users who can either contribute or are editor.


function findAdmin(ListOfUsers)
{
	let arr = []
	ListOfUsers.forEach((element)=>{

		const {name, rights} = element
		for(let key of rights)
		{
		if(key == "admin"){
			arr.push(name)
		}
	}
	})
	console.log("problem 1", arr)
}

function usersAbove18AndContributers(ListOfUsers){
	let arr = []
	ListOfUsers.forEach((element)=>{
		const{name, age, rights} = element
		for(let key of rights)
		{
		  if(key == "contributor" && age > 18){
			arr.push(name)
		  }
	   }
	})
	console.log("problem 2", arr)
}


function havingAllRights(ListOfUsers){
	//let arr = []
	ListOfUsers.forEach((element)=>{
		const{name, rights} = element
		    
			if( rights.indexOf("admin") !== -1 && rights.indexOf("editor") !== -1 && rights.indexOf("editor") !== -1){
				console.log("problem 3", name)
			}
	})
}


function sortBasedOnAdmin(ListOfUsers){
	ListOfUsers.sort((a,b)=>{
		if(a.rights < b.rights){
			return -1
		}
		if(a.rights>b.rights){
			return 1
		}
		else{
			return 0
		}
	})
	console.log("problem 4", ListOfUsers)
}


function userEitherContributerOrEditor(ListOfUsers){
	let arr = []
	ListOfUsers.forEach((element)=>{
		const{name, rights} = element
		    
			if( (rights.indexOf("contributor") !== -1 && rights.indexOf("editor") == -1) || (rights.indexOf("contributor") == -1 && rights.indexOf("editor") !== -1)){
				console.log("problem 5", name)
			}
	})
}

// findAdmin(ListOfUsers)
// usersAbove18AndContributers(ListOfUsers)
// havingAllRights(ListOfUsers)
// sortBasedOnAdmin(ListOfUsers)
// userEitherContributerOrEditor(ListOfUsers)

module.exports = {findAdmin,usersAbove18AndContributers,havingAllRights,sortBasedOnAdmin,userEitherContributerOrEditor}