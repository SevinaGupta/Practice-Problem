const test = require("./solution.js")

const ListOfUsers = [{
    "name": "Jane",
    "age": 36,
    "rights": ["admin", "editor", "contributor"]
}, {
    "name": "Emily",
    "age": 23,
    "rights": ["editor"]
},
{
    "name": "Paul",
    "age": 44,
    "rights": ["contributor"]
}, {
    "name": "Mark",
    "age": 26,
    "rights": ["contributor"]
},
{
    "name": "Edward",
    "age": 39,
    "rights": ["admin"]
}, {
    "name": "Lucy",
    "age": 23,
    "rights": ["admin", "editor", "contributor"]
}]

test.findAdmin(ListOfUsers)
test.usersAbove18AndContributers(ListOfUsers)
test.havingAllRights(ListOfUsers)
test.sortBasedOnAdmin(ListOfUsers)
test.userEitherContributerOrEditor(ListOfUsers)